module gitlab.com/makers-page/utils

go 1.20

require (
	github.com/google/uuid v1.3.0
	github.com/klauspost/compress v1.16.7
	github.com/oklog/ulid/v2 v2.1.0
)
